# Twinhead Raid Log Scraper #

A python program to scrape, parsem and objectify the data from a Twinhead kill log. (This functions only for the Kronos Private WoW Server)

The program implements batch and range jobs with conditional operations to filter the collected data (e.g. kill uids 556011 to 556022 where guild == Vanguard)

## help() ##

Twinhead Scraper

Syntax:

```
 ./twinhead_scrape {url or operation type} [{operation parameters}[{etc.}]] [{conditional subject}{conditional actor}]
```


 <url>

   The URL of a Twinhead kill log link

   e.g. ./twinhead_scrape https://vanilla-twinhead.twinstar.cz/?boss-kill=551368

 **batch** {txt file of Twinhead kill log URLs}

   Performs a batch operation over a text file of Twinhead URLs

   e.g. ./twinhead_scrape ~/you/doc/urls.txt

 **range** {start} {end}

   Performs a batch operation defined by the ascending IDs of Twinhead kill UIDs, which generates the URLs to scrape

   e.g. ./twinhead_scrape 551357 551381

### CONDITIONS ###

The batch and range operations implement an optional conditional operation that can be used at the end of the initial call to the program. Conditional operations are performed using pairs of statements following the specification of an operation type. The first part of a conditional test pair is the subject: the field to test. The second part is the actor: the value expected. The subject is not case sensitive, and is chosen from a finite list of possible subjects. The object is case sensitive but given by the user.

e.g. ./twinhead_scrape batch ~/doc/file.txt GuiLd ONSLAUGHT 

     ./twinhead_scrape batch ~/doc/file.txt death_count 0 instance "Ahn'Qiraj Temple"

     ./twinhead_scrape range 551357 551399 guilD Vanguard

The list of possible subjects is finite. The possible subjects are:


```
  bossname   realm
  kill_uid   killed_at
  instance   wipe_count
  guild      death_count
```


### PRINTOUTS ###

The type of printout given is defined by a constant value given at the start of the python file: PRNTMDE. Two options are available:

  OBJ - Gives a pretty output of kill-specific details for matching kills.

  RAW - Dumps the raw fight data for matching kills and nothing else.

RAW is the default value of this switch