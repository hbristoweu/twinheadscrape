#!/usr/bin/env python3
# Requires: lxml, requests
# $ pip install lxml
# $ pip install requests
from lxml import html
import os
import requests
import re
import sys



# Sample killdata - ONSLAUGHT vs Cthun, 2017/05/19 04:14:56
SMPLURL = """https://vanilla-twinhead.twinstar.cz/?boss-kill=551381"""

# Printmode, to be replaced by cmdline args
PRNTMDE = "RAW"	# Options: RAW, OBJ as string



def debug():
  scrapeAndParse(SMPLURL)



def main():
  if (len(sys.argv) > 1):
    if (sys.argv[1] == "batch"):
      if (len(sys.argv) > 3): # Conditional statements are present
        batchJob(sys.argv[2], sys.argv[3:])
      else:
        batchJob(sys.argv[2])
    elif (sys.argv[1] == "range"):
      if (len(sys.argv) < 4):
        print("Not enough parameters.")
      else:
        if (len(sys.argv) > 4): # Conditional statements are present
         rangeScrape(int(sys.argv[2]), int(sys.argv[3]), sys.argv[4:])
    elif (sys.argv[1] == "help"):
      help()
    else:
      scrapeAndParse(sys.argv[1])
  else:
    print("No url given. Please give the URL of a twinhead kill log.")



def printOperation(killDetailsObj):
  if PRNTMDE == "OBJ":
    killDetailsObj.presentData()
  if PRNTMDE == "RAW":
    #print(killDetailsObj.raw_data)
    killDetailsObj.presentRawData()
    #print("[Raw Killdata here]")



def rangeScrape(startx, endx, conditions=""):
  for x in range(startx, endx):
    killDetailsObj = scrapeAndParse("""https://vanilla-twinhead.twinstar.cz/?boss-kill=""" + str(x))
    if killDetailsObj:
      if conditionTest(killDetailsObj, conditions):
        printOperation(killDetailsObj)



def batchJob(pathToTxt, conditions=""):
  with open(pathToTxt) as f:
    for line in f:
      if line.strip():
        killDetailsObj = scrapeAndParse(line.strip())
        if killDetailsObj:
          if conditionTest(killDetailsObj, conditions):
            printOperation(killDetailsObj)



def conditionTest(killDetailsObj, conditions):
  if conditions == "" or len(conditions) & 1 == 1:	# isOdd
    return True
  else:

    bool_succeed = True    

    for x in range(0, len(conditions), 2):
      conditions[x] = conditions[x].lower()

      if (conditions[x] == "bossname"):
        bool_succeed = killDetailsObj.bossname == conditions[x+1]
      elif (conditions[x] == "kill_uid"):
        bool_succeed = killDetailsObj.kill_uid == conditions[x+1]
      elif (conditions[x] == "instance"):
        bool_succeed = killDetailsObj.instance == conditions[x+1]
      elif (conditions[x] == "guild"):
        bool_succeed = killDetailsObj.guild == conditions[x+1]
      elif (conditions[x] == "realm"):
        bool_succeed = killDetailsObj.realm == conditions[x+1]
      elif (conditions[x] == "killed_at"):
        bool_succeed = killDetailsObj.killed_at == conditions[x+1]
      elif (conditions[x] == "wipe_count"):
        bool_succeed = killDetailsObj.wipe_count == conditions[x+1]
      elif (conditions[x] == "death_count"):
        bool_succeed = killDetailsObj.death_count == conditions[x+1]

      if not bool_succeed:
        if PRNTMDE == "RAW":
          #print("")
          pass
        else:
          #print("Condition failed, skipping", killDetailsObj.kill_uid, " (", conditions[x], " ?= ", conditions[x+1], " was not true)")
          pass
        return False

    return bool_succeed



def scrapeBossKillDetails(meat, killDetailsObj):
  page_killdetails = meat.xpath('//*[@id="main-contents"]/div[1]/div[2]')
  body_killdetails = page_killdetails[0][0].text_content().splitlines()
  raw_killdetails = list()
  for line in body_killdetails:
    if line.strip():
      raw_killdetails.append(line.lstrip())
  
  killDetailsObj.bossname = raw_killdetails[1]
  killDetailsObj.instance = raw_killdetails[3]
  killDetailsObj.guild = raw_killdetails[5]
  killDetailsObj.realm = raw_killdetails[7]
  killDetailsObj.killed_at = raw_killdetails[9]
  killDetailsObj.wipe_count = raw_killdetails[11]
  killDetailsObj.death_count = raw_killdetails[13]
  #Erminn's duration-to-seconds request
  tmp = list(filter(None, [x for x in re.split('(min|sec)', raw_killdetails[15])]))
  killDetailsObj.duration = round(float(tmp[0])*60, 2)
  if len(tmp) > 2: killDetailsObj.duration += float(tmp[2])
  
  killDetailsObj.resurrections = raw_killdetails[19]
  killDetailsObj.loot = raw_killdetails[22::2]

  return killDetailsObj



def scrapeBossKilldata(meat, killDetailsObj):
  page_killdata = meat.xpath('//*[@id="main-contents"]/script[1]/text()')[0]
  killdata = re.search('\[\{"guid.*\]\;', page_killdata).group()
  killDetailsObj.raw_data = killdata
  
  return killDetailsObj



def scrapeAndParse(url):
  page = requests.get(url)
  meat = html.fromstring(page.content)

  isBosskill = meat.xpath('//*[@id="main-contents"]/div[1]/h1')[0]
  if (isBosskill.text_content() == "Latest Boss Kills"):
    return False

  thisKill = KillDetails()

  thisKill.kill_uid = re.search('\d+$', url).group()

  scrapeBossKillDetails(meat, thisKill)
  scrapeBossKilldata(meat, thisKill)

  return thisKill



class KillDetails:
  kill_uid = 0
  bossname = ""
  instance = ""
  guild = ""
  realm = ""
  killed_at = ""
  wipe_count = ""
  death_count = 0
  duration = ""
  resurrections = 0
  loot = []
  raw_data = ""

  def presentRawData(this):
    sys.stdout.write("[{\"kill_uid\":\""+ this.kill_uid+"\",")
    sys.stdout.write("\"bossname\":\""+ this.bossname+"\",")
    sys.stdout.write("\"instance\":\""+ this.instance+"\",")
    sys.stdout.write("\"guild\":\""+ this.guild+"\",")
    sys.stdout.write("\"realm\":\""+ this.realm+"\",")
    sys.stdout.write("\"killed_at\":\""+ this.killed_at+"\",")
    sys.stdout.write("\"wipe_count\":\""+ this.wipe_count+"\",")
    sys.stdout.write("\"death_count\":\""+ this.death_count+"\",")
    sys.stdout.write("\"resurrections\":\""+ this.resurrections+"\",")
    sys.stdout.write("\"loot\":\""+ str(this.loot)+"\"}]\n")
    print(this.raw_data)

  def presentData(this):
    print("Kill UID\t", this.kill_uid)
    
    print("Bossname\t", this.bossname)
    print("Instance\t",this.instance)
    print("Guild\t\t",this.guild)
    print("Realm\t\t",this.realm)
    print("Killed at\t",this.killed_at)
    print("Wipe Count\t",this.wipe_count)
    print("Death Count\t",this.death_count)
    print("Duration\t",this.duration)
    print("Resurrections\t",this.resurrections)
    print("Loot",this.loot)
    
    #print(this.raw_data[:256]) # Contracted for Succinct testing



def help():
  print('''Twinhead Scraper

Syntax:
 ./twinhead_scrape {url or operation type} [{operation parameters}[{etc.}]] [{conditional subject}{conditional actor}]

 <url>
   The URL of a Twinhead kill log link
   e.g. ./twinhead_scrape https://vanilla-twinhead.twinstar.cz/?boss-kill=551368
 batch {txt file of Twinhead kill log URLs}
   Performs a batch operation over a text file of Twinhead URLs
   e.g. ./twinhead_scrape ~/you/doc/urls.txt
 range {start} {end}
   Performs a batch operation defined by the ascending IDs of Twinhead kill UIDs, which generates the URLs to scrape
   e.g. ./twinhead_scrape 551357 551381

CONDITIONS
----------

The batch and range operations implement an optional conditional operation that 
can be used at the end of the initial call to the program. Conditional 
operations are performed using pairs of statements following the specification 
of an operation type. The first part of a conditional test pair is the subject: 
the field to test. The second part is the actor: the value expected. The subject 
is not case sensetive, and is chosen from a finite list of possible subjects. 
The object is case sensetive but given by the user.

e.g. ./twinhead_scrape batch ~/doc/file.txt GuiLd ONSLAUGHT 
     ./twinhead_scrape batch ~/doc/file.txt death_count 0 instance "Ahn'Qiraj 
Temple"
     ./twinhead_scrape range 551357 551399 guilD Vanguard

The list of possible subjects are finite and are:

  bossname   realm
  kill_uid   killed_at
  instance   wipe_count
  guild      death_count

PRINTOUTS
----------

The type of printout given is defined by a constant value given at the start of 
the python file: PRNTMDE. Two options are available:
  OBJ - Gives a pretty output of kill-specific details for matching kills.
  RAW - Dumps the raw fight data for matching kills and nothing else.

RAW is the default value of this switch
''')



if __name__ == "__main__":
  main()
  #debug()
  #help()
  
  



